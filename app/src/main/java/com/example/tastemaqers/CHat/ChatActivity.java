package com.example.tastemaqers.CHat;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.tastemaqers.DJ.DJ_details;
import com.example.tastemaqers.DJ.DJ_displayActivity;
import com.example.tastemaqers.DJ.MainActivity;
import com.example.tastemaqers.DJ.djAdaptor;
import com.example.tastemaqers.Misc.BottomNavClass;
import com.example.tastemaqers.Misc.StreamActivity;
import com.example.tastemaqers.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ServerValue;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.protobuf.StringValue;

import java.util.HashMap;
import java.util.Map;

public class ChatActivity extends AppCompatActivity {

    private static final String TAG = "ChatActivity";
    private BottomNavigationView bottomNavigationView;
    private BottomNavClass bottomNavClass;
    private FloatingActionButton fab;
    private RecyclerView chatRecyclerView;
    private ChatAdaptor chatAdaptor;
    private ImageView sendMessage;
    private EditText messageText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavChat);
        messageText = (EditText) findViewById(R.id.message);
        sendMessage = (ImageView) findViewById(R.id.sendMessage);
        initBottomNav();
        initRecyclerView();

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(messageText.getText())) {
                    Map<String, Object> messageData = new HashMap<>();
                    messageData.put("message", messageText.getText().toString());
                    messageData.put("userUUID", FirebaseAuth.getInstance().getUid());
                    messageData.put("username", FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                    messageData.put("timestamp",  FieldValue.serverTimestamp());


                    FirebaseFirestore.getInstance().collection("ChatRoom")
                            .add(messageData)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                                    Toast.makeText(getApplicationContext(),"Message send",Toast.LENGTH_LONG).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error adding document", e);
                                    Toast.makeText(getApplicationContext(),"Message not send",Toast.LENGTH_LONG).show();
                                }
                            });

                }
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.streamingService1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), StreamActivity.class));
            }
        });
    }

    private void initBottomNav() {
        bottomNavigationView.setSelectedItemId(R.id.ItemChat);
        bottomNavClass = new BottomNavClass();
        bottomNavClass.BottomInit(ChatActivity.this,bottomNavigationView);
    }
    private void initRecyclerView() {
        Log.d(TAG, "initRecyclerView: started");
        Query query = FirebaseFirestore.getInstance().collection("ChatRoom").orderBy("timestamp");
        FirestoreRecyclerOptions<chatClass> options = new FirestoreRecyclerOptions.Builder<chatClass>()
                .setQuery(query,chatClass.class)
                .build();
        chatAdaptor = new ChatAdaptor(options, ChatActivity.this);
        chatRecyclerView = (RecyclerView) findViewById(R.id.chatRecyclerView);
        chatRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        chatRecyclerView.setAdapter(chatAdaptor);
        Log.d(TAG, "initRecyclerView: ended");


    }
    @Override
    protected void onStart() {
        super.onStart();
        chatAdaptor.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        chatAdaptor.stopListening();
    }
}
