package com.example.tastemaqers.CHat;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tastemaqers.DJ.djAdaptor;
import com.example.tastemaqers.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

class ChatAdaptor extends FirestoreRecyclerAdapter<chatClass,ChatAdaptor.chatHolder> {

    private static final String TAG = "ChatAdaptor";
    private Context context;
    public ChatAdaptor(@NonNull FirestoreRecyclerOptions<chatClass> options, Context con) {
        super(options);
        this.context = con;
    }

    @Override
    protected void onBindViewHolder(@NonNull chatHolder holder, int position, @NonNull chatClass model) {
        holder.userName.setText(model.getUsername());
        holder.message.setText(model.getMessage());
        Log.d(TAG, "FirebaseAuth.getInstance().getUid() = " +FirebaseAuth.getInstance().getUid()+ "model.getuserUUID(): "+model.getUserUUID() );
        if(model.getUserUUID().matches(FirebaseAuth.getInstance().getUid())){
            holder.messageBubble.setCardBackgroundColor(Color.parseColor("#212d3a"));
            holder.userName.setTextColor(Color.parseColor("#ffffff"));
            holder.message.setTextColor(Color.parseColor("#ffffff"));
        }
    }

    @NonNull
    @Override
    public chatHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_messages_layout,viewGroup,false);
        return new chatHolder(view);
    }

    class chatHolder extends RecyclerView.ViewHolder {
        private TextView userName, message;
        private CardView messageBubble;
        public chatHolder(@NonNull View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.chat_userId);
            message = (TextView) itemView.findViewById(R.id.chat_message);
            messageBubble = (CardView) itemView.findViewById(R.id.messageBubble);

        }
    }{

    }
}
