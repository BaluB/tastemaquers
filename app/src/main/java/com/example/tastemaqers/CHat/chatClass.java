package com.example.tastemaqers.CHat;

public class chatClass {

    public chatClass(){}

    public chatClass(String message,  String userUUID, String username) {
        this.message = message;
        this.userUUID = userUUID;
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public String getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    String message,userUUID,username;
}
