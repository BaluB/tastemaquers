package com.example.tastemaqers.DJ;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class DJ_details  {
    String name, type,details,image;

    public DJ_details() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public DJ_details(String name, String type, String details, String image) {

        this.name = name;
        this.type = type;
        this.details = details;
        this.image = image;
    }


}
