package com.example.tastemaqers.DJ;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.tastemaqers.Misc.BottomNavClass;
import com.example.tastemaqers.Misc.MyService;
import com.example.tastemaqers.Misc.StreamActivity;
import com.example.tastemaqers.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class DJ_displayActivity extends AppCompatActivity {

    private static final String TAG = "DJ_displayActivity";
    private BottomNavigationView bottomNavigationView;
    private BottomNavClass bottomNavClass;
    private RecyclerView recyclerView;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference DjRef = db.collection("DJ_Profiles");
    private djAdaptor adaptor;
    private FirebaseAuth firebaseAuth;
    private FloatingActionButton fab;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.activity_dj_display);bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNav);
        firebaseAuth = FirebaseAuth.getInstance();
        initBottomNav();
        initRecyclerView();
        fab = (FloatingActionButton) findViewById(R.id.streamingService);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), StreamActivity.class));
            }
        });

    }


    private void initRecyclerView() {
        Log.d(TAG, "initRecyclerView: started");
        Query query = FirebaseFirestore.getInstance().collection("DJ_Profiles");
        FirestoreRecyclerOptions<DJ_details> options = new FirestoreRecyclerOptions.Builder<DJ_details>()
                .setQuery(query,DJ_details.class)
                .build();
        adaptor = new djAdaptor(options,DJ_displayActivity.this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adaptor);
        Log.d(TAG, "initRecyclerView: ended");


    }


    private void initBottomNav() {
        bottomNavigationView.setSelectedItemId(R.id.ItemDJ);
        bottomNavClass = new BottomNavClass();
        bottomNavClass.BottomInit(DJ_displayActivity.this,bottomNavigationView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adaptor.startListening();
        if(firebaseAuth.getCurrentUser() == null){
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        adaptor.stopListening();
    }
}