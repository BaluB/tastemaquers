package com.example.tastemaqers.DJ;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tastemaqers.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class ProfileActivity extends AppCompatActivity {
    private static final String TAG = "ProfileActivity";
    String data;
    private TextView name,dob,desc,origin,occupation,generes;
    private ImageView profileImg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        data = getIntent().getStringExtra("data");
        Log.d(TAG, "onCreate: "+ data);
        profileImg = (ImageView)findViewById(R.id.dj_image);
        name = (TextView) findViewById(R.id.dj_name);
        dob = (TextView) findViewById(R.id.dj_dob);
        desc = (TextView) findViewById(R.id.dj_desc);
        origin = (TextView) findViewById(R.id.dj_origin);
        occupation = (TextView) findViewById(R.id.dj_occupation);
        generes = (TextView) findViewById(R.id.dj_generes);
        getData();
    }

    private void getData() {
        DocumentReference docRef = FirebaseFirestore.getInstance().collection("DJ_Profiles").document(data);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        Glide.with(ProfileActivity.this).load(document.getString("image")).into(profileImg);
                        name.setText(document.getString("name"));
                        dob.setText(document.getString("dob"));
                        desc.setText(document.getString("desc"));
                        origin.setText(document.getString("origin"));
                        occupation.setText(document.getString("occupation"));
                        generes.setText(document.getString("generes"));
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }
}
