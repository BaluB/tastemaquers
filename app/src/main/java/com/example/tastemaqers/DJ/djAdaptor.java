package com.example.tastemaqers.DJ;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tastemaqers.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;


public class djAdaptor extends FirestoreRecyclerAdapter<DJ_details,djAdaptor.Dj_holder> {

    private static final String TAG = "djAdaptor";
    private Context context;
    public djAdaptor(@NonNull FirestoreRecyclerOptions<DJ_details> options,Context con) {
        super(options);
        this.context = con;
    }

    @Override
    protected void onBindViewHolder(@NonNull final Dj_holder holder, int position, @NonNull final DJ_details model) {
        Log.d(TAG, "onBindViewHolder: name : "+ model.getName() + "details: "+ model.getDetails() + "type: "+ model.getType());
        holder.name.setText(model.getName());
        holder.details.setText(model.getDetails());
        holder.type.setText(model.getType());
        holder.read_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DocumentSnapshot snapshot = getSnapshots().getSnapshot(holder.getAdapterPosition());
                Intent intent = new Intent(context,ProfileActivity.class);
                intent.putExtra("data",snapshot.getId());
                context.startActivity(intent);
            }
        });
        Glide
                .with(context)
                .load(model.getImage())
                .into(holder.djPic);
    }

    @NonNull
    @Override
    public Dj_holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dj_recycler_items,viewGroup,false);
        return new Dj_holder(view);
    }

    class Dj_holder extends RecyclerView.ViewHolder {
        TextView name,details,type,read_more;
        ImageView djPic;
        public Dj_holder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.dj_dob);
            details = (TextView) itemView.findViewById(R.id.dj_details);
            type = (TextView) itemView.findViewById(R.id.dj_type);
            read_more = (TextView) itemView.findViewById(R.id.readMore);
            djPic = (ImageView) itemView.findViewById(R.id.dj_Pic);

        }
    }
}
