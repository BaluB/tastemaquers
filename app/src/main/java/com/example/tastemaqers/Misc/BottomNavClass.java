package com.example.tastemaqers.Misc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;

import com.example.tastemaqers.CHat.ChatActivity;
import com.example.tastemaqers.DJ.DJ_displayActivity;
import com.example.tastemaqers.DJ.MainActivity;
import com.example.tastemaqers.R;
import com.example.tastemaqers.Shows.ShowsActivity;

public class BottomNavClass {

    public void BottomInit(final Context context, final BottomNavigationView bottomNavigationView){
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch(menuItem.getItemId()){

                    case R.id.ItemDJ:
                        context.startActivity(new Intent(context, DJ_displayActivity.class));
                        ((Activity)context).overridePendingTransition(0,0);
                        ((Activity)context).finish();
                        return true;
                    case R.id.ItemChat:
                        context.startActivity(new Intent(context, ChatActivity.class));
                        ((Activity)context).overridePendingTransition(0,0);
                        ((Activity)context).finish();
                        return true;
                    case R.id.ItemShows:
                        context.startActivity(new Intent(context, ShowsActivity.class));
                        ((Activity)context).overridePendingTransition(0,0);
                        ((Activity)context).finish();
                        return true;
                        default:
                            return false;

                }
            }
        });


    }
}
