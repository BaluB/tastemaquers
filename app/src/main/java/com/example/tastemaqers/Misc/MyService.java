package com.example.tastemaqers.Misc;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import static com.example.tastemaqers.DJ.MainActivity.TAG;

public class MyService extends Service {
    //creating a mediaplayer object
    private MediaPlayer player;
    private String StreamLink = "https://s1.vocaroo.com/media/download_temp/Vocaroo_s1EKv4Swy2VA.mp3";
    //    private String StreamLink =  "http://192.168.0.28:8000/stream";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        DocumentReference docRef = FirebaseFirestore.getInstance().collection("StreamDetails").document("details");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        player = MediaPlayer.create(getApplicationContext(), Uri.parse(document.getString("stream_link")));
                        player.start();
                        } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        //stopping the player when service is destroyed
        player.stop();
    }
}