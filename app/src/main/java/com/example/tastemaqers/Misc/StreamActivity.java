package com.example.tastemaqers.Misc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tastemaqers.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.concurrent.TimeUnit;

import dyanamitechetan.vusikview.VusikView;

public class StreamActivity extends AppCompatActivity /*implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener*/ {

    private static final String TAG = "StreamActivity";
    private String StreamLink = "https://s1.vocaroo.com/media/download_temp/Vocaroo_s1EKv4Swy2VA.mp3";
    //    private String StreamLink =  "http://192.168.0.28:8000/stream";

    private VusikView musicView;
    private ImageView play, pause,albumArt;
    private TextView streamSong;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream);//getting buttons from xml
        play = (ImageView) findViewById(R.id.playBtn);
        pause = (ImageView) findViewById(R.id.pauseBtn);
        albumArt = (ImageView) findViewById(R.id.albumart);
        streamSong = (TextView) findViewById(R.id.songName);

        DocumentReference docRef = FirebaseFirestore.getInstance().collection("StreamDetails").document("details");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Glide.with(getApplicationContext())
                                .load(document.getString("album_art"))
                                .into(albumArt);
                        streamSong.setText(document.getString("playing"));
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });



        musicView = (VusikView)findViewById(R.id.musicView);
        musicView.start();
        //attaching onclicklistener to buttons
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(new Intent(getApplicationContext(), MyService.class));


            }
        });
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(new Intent(getApplicationContext(), MyService.class));



            }
        });
    }
}





















































