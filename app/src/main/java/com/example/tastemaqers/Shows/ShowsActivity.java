package com.example.tastemaqers.Shows;

import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.widget.LinearLayout;

import com.example.tastemaqers.Misc.BottomNavClass;
import com.example.tastemaqers.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class ShowsActivity extends AppCompatActivity {

    private static final String TAG = "ShowsActivity";
    private BottomNavigationView bottomNavigationView;
    private BottomNavClass bottomNavClass;
    private RecyclerView morningRecyclerView,noonRecyclerView;
    private showAdaptor showAdaptor,showAdaptor1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shows);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavShow);
        initBottomNav();
        initRecyclerView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    private void initRecyclerView() {

        //morningRecyclerView
        Log.d(TAG, "initRecyclerView: started");
        Query query = FirebaseFirestore.getInstance().collection("Radio").document("Morning").collection("songs");
        FirestoreRecyclerOptions<showclass> options = new FirestoreRecyclerOptions.Builder<showclass>()
                .setQuery(query,showclass.class)
                .build();
        showAdaptor = new showAdaptor(options,getApplicationContext());
        morningRecyclerView = (RecyclerView) findViewById(R.id.morningRecyclerview);
        morningRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayout.HORIZONTAL,false));
        morningRecyclerView.setAdapter(showAdaptor);
        Log.d(TAG, "initRecyclerView: ended");

        //noonRecyclerView
        Log.d(TAG, "initRecyclerView: started");
        Query query1 = FirebaseFirestore.getInstance().collection("Radio").document("Noon").collection("songs");
        FirestoreRecyclerOptions<showclass> options1 = new FirestoreRecyclerOptions.Builder<showclass>()
                .setQuery(query1,showclass.class)
                .build();
        showAdaptor1 = new showAdaptor(options1,getApplicationContext());
        noonRecyclerView = (RecyclerView) findViewById(R.id.noonrecyclerView);
        noonRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayout.HORIZONTAL,false));
        noonRecyclerView.setAdapter(showAdaptor1);
        Log.d(TAG, "initRecyclerView: ended");


    }

    private void initBottomNav() {
        bottomNavigationView.setSelectedItemId(R.id.ItemShows);
        bottomNavClass = new BottomNavClass();
        bottomNavClass.BottomInit(ShowsActivity.this,bottomNavigationView);
    }
    @Override
    protected void onStart() {
        super.onStart();
        showAdaptor.startListening();
        showAdaptor1.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        showAdaptor.stopListening();
        showAdaptor1.startListening();
    }
}