package com.example.tastemaqers.Shows;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tastemaqers.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class showAdaptor extends FirestoreRecyclerAdapter<showclass,showAdaptor.showHolder> {

    private static final String TAG = "showAdaptor";
    private Context context;
    public showAdaptor(@NonNull FirestoreRecyclerOptions<showclass> options, Context con) {
        super(options);
        this.context = con;
    }

    @Override
    protected void onBindViewHolder(@NonNull showHolder holder, int position, @NonNull showclass model) {
        Glide.with(context)
                .load(model.getAlbumArt())
                .into(holder.albumArt);
        holder.artist.setText(model.getArtist());
        holder.songName.setText(model.getSongName());
        holder.time.setText(model.getTime());
        Log.d(TAG, "onBindViewHolder: all done");
    }

    @NonNull
    @Override
    public showHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.schedule_layout,viewGroup,false);
        return new showAdaptor.showHolder(view);
    }

    class showHolder extends RecyclerView.ViewHolder{
        private ImageView albumArt;
        private TextView songName,time,artist;
        public showHolder(@NonNull View itemView) {
            super(itemView);
            albumArt = (ImageView)itemView.findViewById(R.id.sch_album_art);
            songName = (TextView)itemView.findViewById(R.id.sch_song_name);
            time = (TextView)itemView.findViewById(R.id.sch_time);
            artist = (TextView)itemView.findViewById(R.id.sch_artist);
        }
    }
}
