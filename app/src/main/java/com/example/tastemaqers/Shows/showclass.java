package com.example.tastemaqers.Shows;

public class showclass {
    public showclass(String albumArt, String artist, String songName, String time) {
        this.albumArt = albumArt;
        this.artist = artist;
        this.songName = songName;
        this.time = time;
    }
    public showclass(){}


    public String getAlbumArt() {
        return albumArt;
    }

    public void setAlbumArt(String albumArt) {
        this.albumArt = albumArt;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    String albumArt,artist,songName,time;
}
